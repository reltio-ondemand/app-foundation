package com.reltio.hierarchy.validation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.hierarchy.model.json.input.InputModel;
import com.reltio.hierarchy.model.json.report.Report;
import com.reltio.hierarchy.model.json.report.Result;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

public class HierarchyValidationTest {
    private static ObjectMapper mapper = new ObjectMapper();

    @Test
    public void callValidationService() throws Exception {
        doValidate("/options_ProductGroup.json");
    }

    @Test
    public void testAllParamsWithinLimit() throws Exception {
        List<Report> reports = doValidate("/all-parameters-within-limits.json");
        List<Result> results = reports.get(0).getResults();
        for (Result result: results) {
            assertTrue(result.getDescription(), result.getValid());
        }
    }

    @Test
    public void invalidMaxParents() throws Exception {
        String msg = "Too many parent nodes in this hierarchy. Limit is 1";
        validate("/invalid-maxParents.json", "maxParents", msg);
    }

    @Test
    public void invalidMaxRootNodes() throws Exception {
        String msg = "Too many root nodes in this hierarchy. Limit is 1";
        validate("/invalid-maxRootNodes.json", "maxRootNodes", msg);
    }

    @Test
    public void invalidMaxLevels() throws Exception {
        String msg = "Entity is at level 3. Max level allowed is 2";
        validate("/invalid-maxLevels.json", "maxLevels", msg);
    }

    @Test
    public void invalidAssertParentCount() throws Exception {
        String msg = "Parent count must be 2";
        validate("/invalid-assertParentCount.json", "assertParentCount", msg);
    }

    private void validate(String resource, String testName, String expectedMessage) throws Exception {
        List<Report> reports = doValidate(resource);
        Result result = getResult(reports.get(0).getResults(), testName);
        assertNotNull(result);
        assertFalse(result.getDescription(), result.getValid());
        assertEquals(expectedMessage, result.getDescription());
    }

    private List<Report> doValidate(String resource) throws Exception {
        InputStream in = getClass().getResourceAsStream(resource);
        InputModel inputModel = mapper.readValue(in, InputModel.class);

        HierarchyValidation validation = new HierarchyValidation();
        List<Report> reports = validation.doValidate(inputModel);
        return reports;
    }

    private Result getResult(List<Result> results, String testName) {
        for (Result result: results) {
            if (result.getTestName().equals(testName)) return result;
        }

        return null;
    }
}
