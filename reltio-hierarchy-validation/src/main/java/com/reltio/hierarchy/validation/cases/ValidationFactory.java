package com.reltio.hierarchy.validation.cases;

public class ValidationFactory {
    public static Validation getValidation(String testName) {
        if (testName.equals("maxParents")) {
            return new MaxParents();
        } else if (testName.equals("assertParentCount")) {
            return new AssertParentCount();
        } else if (testName.equals("maxRootNodes")) {
            return new MaxRootNodes();
        } else if (testName.equals("maxLevels")) {
            return new MaxLevels();
        }

        throw new UnsupportedOperationException("'" + testName + "' is not supported");
    }
}
