package com.reltio.hierarchy.validation;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.hierarchy.api.ApiHandler;
import com.reltio.hierarchy.model.json.input.InputModel;
import com.reltio.hierarchy.model.json.report.Report;
import com.reltio.hierarchy.model.json.report.Result;

import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class HierarchyValidation
    implements RequestHandler<InputModel, List<Report>> {

    @Override
    public List<Report> handleRequest(InputModel inputModel, Context context) {
        LambdaLogger logger = context.getLogger();
        try {
            return doValidate(inputModel);
        } catch (InterruptedException e) {
            logger.log(e.getMessage());
            throw new RuntimeException(e.getMessage());
        } catch (GenericException e) {
            logger.log(e.getMessage());
            throw new RuntimeException(e.getExceptionMessage());
        } catch (APICallFailureException e) {
            String msg = "Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse();
            logger.log(msg);
            throw new RuntimeException(msg);
        }
    }

    public List<Report> doValidate(InputModel inputModel)
        throws InterruptedException, GenericException, APICallFailureException {
        ApiHandler apiHandler = new ApiHandler(inputModel);

        long start = System.nanoTime();

        ExecutorService executor = Executors.newWorkStealingPool();

        List<Report> reports =
            inputModel.getEntityUri()
                .parallelStream()
                .map(entityUri -> {
                    Callable<List<Result>> validationThread = new ValidationThread(apiHandler, inputModel, entityUri);
                    Future<List<Result>> future = executor.submit(validationThread);

                    // create report
                    Report report = new Report();
                    report.setEntityUri(entityUri);
                    report.setResults(future);

                    return report;
                })
                .collect(Collectors.toList());

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.HOURS);

        long time = System.nanoTime() - start;
        System.out.printf("\nTasks took %.2f ms to run%n", time/1e6);
        
        return reports;
    }
}
