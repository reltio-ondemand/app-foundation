package com.reltio.hierarchy.validation.cases;

import com.reltio.hierarchy.model.node.ChildNode;
import com.reltio.hierarchy.model.json.report.Result;

import java.text.MessageFormat;

public class MaxLevels implements Validation {

    @Override
    public void validate(ChildNode childEntity, com.reltio.hierarchy.model.json.input.Validation validation, Result result) {
        if(childEntity.getLevel() > validation.getCount()) {
            String msg = MessageFormat.format(validation.getMessage(), childEntity.getLevel(), validation.getCount());
            result.setValid(false);
            result.setDescription(msg);
        }
    }
}
