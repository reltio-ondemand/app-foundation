package com.reltio.hierarchy.validation.cases;

import com.reltio.hierarchy.model.node.ChildNode;
import com.reltio.hierarchy.model.json.report.Result;

import java.text.MessageFormat;

public interface Validation {
    void validate(ChildNode childEntity, com.reltio.hierarchy.model.json.input.Validation validation, Result result);

    default void addErrorMessage(com.reltio.hierarchy.model.json.input.Validation validation, Result result) {
        String msg = MessageFormat.format(validation.getMessage(), validation.getCount());
        result.setValid(false);
        result.setDescription(msg);
    }
}
