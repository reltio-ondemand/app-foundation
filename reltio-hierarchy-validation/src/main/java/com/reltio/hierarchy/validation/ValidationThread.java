package com.reltio.hierarchy.validation;

import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.hierarchy.api.ApiHandler;
import com.reltio.hierarchy.model.json.input.InputModel;
import com.reltio.hierarchy.model.json.report.Result;
import com.reltio.hierarchy.model.node.ChildNode;
import com.reltio.hierarchy.model.node.RootNode;
import com.reltio.hierarchy.validation.cases.ValidationFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class ValidationThread implements Callable<List<Result>> {
    private ApiHandler apiHandler;
    private InputModel inputModel;
    private String entityUri;
    public ValidationThread(ApiHandler apiHandler, InputModel inputModel, String entityUri) {
        this.apiHandler = apiHandler;
        this.inputModel = inputModel;
        this.entityUri = entityUri;
    }

    @Override
    public List<Result> call() throws GenericException, IOException, ReltioAPICallFailureException {
        // get the tree node for the entityUri
        RootNode rootNode = apiHandler.getTree(entityUri);

        // find the entity node from the returned tree
        ChildNode childEntity = rootNode.findChild(entityUri);
        if (childEntity == null) return null;

        List<Result> results =
            inputModel.getValidations()
                .parallelStream() // parallel stream
                .filter(validation -> (validation.getCount() != -1)) // filter out tests that are disabled (count = -1)
                .map(validation -> { // perform test and produce result
                    Result result = new Result();
                    result.setTestName(validation.getTestName());
                    ValidationFactory.getValidation(validation.getTestName()).validate(childEntity, validation, result);
                    return result;
                })
                .collect(Collectors.toList());

        // update entities with the result
        apiHandler.storeValidationResults(entityUri, results);

        return results;
    }
}
