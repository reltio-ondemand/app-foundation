package com.reltio.hierarchy.validation.cases;

import com.reltio.hierarchy.model.node.ChildNode;
import com.reltio.hierarchy.model.json.report.Result;

public class MaxRootNodes implements Validation {

    @Override
    public void validate(ChildNode childEntity, com.reltio.hierarchy.model.json.input.Validation validation, Result result) {
        if(childEntity.getRootCount() > validation.getCount()) {
            addErrorMessage(validation, result);
        }
    }
}
