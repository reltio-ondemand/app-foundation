package com.reltio.hierarchy.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.hierarchy.model.json.input.InputModel;
import com.reltio.hierarchy.model.json.report.Result;
import com.reltio.hierarchy.model.node.RootNode;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ApiHandler extends BaseApiHandler {
    public ApiHandler(InputModel inputModel)
        throws GenericException, APICallFailureException {
        super(inputModel);
    }

    /**
     * Call the _tree api for the entity
     *
     * @param entityUri
     * @return
     */
    public RootNode getTree(String entityUri)
        throws GenericException, ReltioAPICallFailureException, IOException {
        String requestUri = getTreeRequestUri(entityUri);
        return new RootNode(get(requestUri));
    }

    public JsonNode getEntity(String entityUri)
        throws GenericException, ReltioAPICallFailureException, IOException {
        String requestUri = getEntityUri(entityUri);
        return get(requestUri);
    }

    /**
     * Add/update Validation Hierarchy Attribute
     *
     * @param entityUri
     * @param results
     */
    public void storeValidationResults(String entityUri, List<Result> results)
        throws IOException, GenericException, ReltioAPICallFailureException {
        // get the entity
        JsonNode entity = getEntity(entityUri);

        // get HV attribute node
        JsonNode hvAttribute = entity.get("attributes").get(getHVAttribute());

        // if HV attribute doesn't exists, add a new HV attribute with all its nested attributes
        if (hvAttribute == null) createHVAttributes(entityUri, results);
        else results.parallelStream().forEach(result -> {
            // if the test was previously not done, add the new test case (attribute)
            if (!hvAttribute.toString().contains(result.getTestName()))
                createHVAttributes(entityUri, Arrays.asList(result));
            else // update the test case (attribute)
                updateHVAttributes(hvAttribute, result);
        });
    }

    private void createHVAttributes(String entityUri, List<Result> results) {
        // create the entity
        try {
            String requestUri = addAttributeRequestUri(entityUri);
            post(requestUri, mapper.writeValueAsString(prepareRequestBody(results)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (ReltioAPICallFailureException e) {
            String msg = "Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse();
            throw new RuntimeException(msg);
        } catch (GenericException e) {
            throw new RuntimeException(e.getExceptionMessage());
        }

    }

    private void updateHVAttributes(String attributeUri, List<Result> results) {
        // update the entity
        try {
            String requestUri = updateAttributeRequestUri(attributeUri);
            put(requestUri, mapper.writeValueAsString(prepareRequestBody(results)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (ReltioAPICallFailureException e) {
            String msg = "Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse();
            throw new RuntimeException(msg);
        } catch (GenericException e) {
            throw new RuntimeException(e.getExceptionMessage());
        }

    }

    private void updateHVAttributes(JsonNode hvAttribute, Result result) {
        hvAttribute.forEach(node -> {
            JsonNode testName = node.get("value").get("TestName");
            if (testName.get(0).get("value").asText().equals(result.getTestName())) {
                // if there is some change in result from previous test, update the attribute
                if (!result.match(node)) updateHVAttributes(node.get("uri").asText(), Arrays.asList(result));
            }
        });
    }
}
