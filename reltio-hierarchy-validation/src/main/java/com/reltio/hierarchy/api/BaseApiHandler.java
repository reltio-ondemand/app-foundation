package com.reltio.hierarchy.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.hierarchy.auth.Authorize;
import com.reltio.hierarchy.model.json.input.InputModel;
import com.reltio.hierarchy.model.json.report.Result;
import com.reltio.hierarchy.model.json.request.hvattribute.add.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BaseApiHandler {
    private final String SEPARATOR = "/";
    protected final ObjectMapper mapper = new ObjectMapper();
    protected InputModel inputModel;
    private ReltioAPIService reltioAPIService;
    private String tenantApiUrl;

    public BaseApiHandler(InputModel inputModel)
        throws APICallFailureException, GenericException {
        this.inputModel = inputModel;
        Authorize auth = new Authorize(inputModel.getUsername(), inputModel.getPassword());
        this.reltioAPIService = new SimpleReltioAPIServiceImpl(auth.getTokenGeneratorService());
        this.tenantApiUrl = inputModel.getEnvUrl() + "/reltio/api/" + inputModel.getTenantId();
    }

    public JsonNode get(String requestUri)
        throws GenericException, ReltioAPICallFailureException, IOException {
        String response = reltioAPIService.get(requestUri);
        return mapper.readTree(response);
    }

    public void put(String requestUri, String requestBody)
        throws GenericException, ReltioAPICallFailureException {
        reltioAPIService.put(requestUri, requestBody);
    }

    public void post(String requestUri, String requestBody)
        throws GenericException, ReltioAPICallFailureException {
        reltioAPIService.post(requestUri, requestBody);
    }

    protected String getEntityUri(String entityUri) {
        return tenantApiUrl + SEPARATOR + entityUri;
    }

    protected String getTreeRequestUri(String entityURI) {
        return getEntityUri(entityURI) + SEPARATOR + "_tree?" + getTreeOption(inputModel.getGraphTypeUri());
    }

    private String getTreeOption(List<String> graphTypeUri) {
        String option = "select=uri,label&graphTypeURIs=";
        option += graphTypeUri.stream().collect(Collectors.joining(","));
        return option;
    }

    protected String getHVAttribute() {
        String entityAttributeUri = inputModel.getEntityAttributeUri();
        return entityAttributeUri.substring(entityAttributeUri.lastIndexOf(SEPARATOR)+1);
    }

    protected String addAttributeRequestUri(String entityURI) {
        return getEntityUri(entityURI) + SEPARATOR + "attributes" + SEPARATOR
                + getHVAttribute();
    }

    protected String updateAttributeRequestUri(String attributeUri) {
        return tenantApiUrl + SEPARATOR + attributeUri;
    }

    protected List<Request> prepareRequestBody(List<Result> results) {
        return
            results.parallelStream()
                .map(result -> {
                    List<TestName> testNames = Arrays.asList(new TestName(result.getTestName()));
                    List<IsValid> validities = Arrays.asList(new IsValid(result.getValid()));
                    List<Description> descriptions = Arrays.asList(new Description(result.getDescription()));

                    Request request = new Request();
                    request.setValue(new Value(testNames, validities, descriptions));

                    return request;
                })
                .collect(Collectors.toList());
    }
}
