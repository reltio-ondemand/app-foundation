
package com.reltio.hierarchy.model.json.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entityUri",
    "label",
    "results"
})
public class Report {
    @Getter @Setter
    @JsonProperty("entityUri")
    private String entityUri;

    @Getter @Setter
    @JsonProperty("label")
    private String label;

    @Setter
    @JsonProperty("results")
    private Future<List<Result>> results;

    public List<Result> getResults() throws ExecutionException, InterruptedException {
        return results.get();
    }
}
