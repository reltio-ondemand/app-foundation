
package com.reltio.hierarchy.model.json.request.hvattribute.add;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Description {
    @JsonProperty("value")
    private String value;
}
