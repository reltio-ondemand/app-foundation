package com.reltio.hierarchy.model.node;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public abstract class TreeNode {

    private JsonNode node;
    private int level = -1;

    public TreeNode(JsonNode node) {
        this.node = node;
    }

    protected JsonNode getNode() {
        return node;
    }

    public int getLevel() {
        return level;
    }

    protected void setLevel(int level) {
        this.level = level;
    }

    public String getUri() {
        return getNode().get("entity").get("uri").textValue();
    }

    public String getType() {
        return getNode().get("entity").get("type").textValue();
    }

    public String getLabel() {
        return getNode().get("entity").get("label").textValue();
    }

    abstract List<ChildNode> getChildren();

    public ChildNode findChild(String entityURI) {
        foundChild = null;
        return findChild(entityURI, getChildren());
    }

    private ChildNode foundChild;
    private ChildNode findChild(String entityURI, List<ChildNode> children) {
        if (children == null || children.isEmpty()) {
            throw new RuntimeException("Can't find a child from empty list of children");
        }
        for (ChildNode child: children) {
            if (child.getUri().equals(entityURI)) {
                foundChild = child;
                return foundChild;
            }

            List<ChildNode> grandChildren = child.getChildren();
            if (grandChildren != null && !grandChildren.isEmpty()) {
                return findChild(entityURI, grandChildren);
            }
        }

        return foundChild;
    }
}
