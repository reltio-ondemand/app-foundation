package com.reltio.hierarchy.model.node;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

public class ChildNode extends TreeNode {
    private boolean leafNode;
    private List<String> parents = new ArrayList<>();
    private List<ChildNode> children = new ArrayList<>();
    private int rootCount = 1;

    public ChildNode(JsonNode child) {
        super(child);
        checkParents();
        checkLeaf();
    }

    private void checkParents() {
        JsonNode parents = getChild().get("parent");
        if (parents.isArray()) {
            for (JsonNode parent: parents) {
                this.parents.add(parent.textValue());
            }
        } else {
            this.parents.add(parents.textValue());
        }
    }

    private void checkLeaf() {
        int totalChild = getChild().get("total").asInt();
        if (totalChild == 0 && getChild().get("children") == null) {
            this.leafNode = true;
        }
    }

    public boolean isLeafNode() {
        return leafNode;
    }

    public JsonNode getChild() {
        return getNode();
    }

    public List<String> getParents() {
        return this.parents;
    }

    public int getParentCount() {
        return getParents().size();
    }

    @Override
    public List<ChildNode> getChildren() {
        if (leafNode) return null;
        if (!children.isEmpty()) return children;

        JsonNode children = getChild().get("children");
        if (children != null) {
            for (JsonNode child : children) {
                ChildNode childNode = new ChildNode(child);
                childNode.setLevel(this.getLevel() + 1);
                this.children.add(childNode);

                childNode.setRootCount(
                    childNode.getParentCount() > this.getRootCount()
                        ? childNode.getParentCount()
                        : this.getRootCount());

            }
        }

        return this.children;
    }

    public int getRootCount() {
        return rootCount;
    }

    public void setRootCount(int rootCount) {
        this.rootCount = rootCount;
    }
}
