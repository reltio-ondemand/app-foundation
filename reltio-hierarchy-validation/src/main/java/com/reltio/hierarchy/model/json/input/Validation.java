
package com.reltio.hierarchy.model.json.input;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Validation {
    @JsonProperty("testName")
    private String testName;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("message")
    private String message;
}
