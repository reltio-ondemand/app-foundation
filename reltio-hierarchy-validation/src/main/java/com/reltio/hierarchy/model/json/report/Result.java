
package com.reltio.hierarchy.model.json.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "testName",
    "isValid",
    "description"
})
public class Result {
    @JsonProperty("testName")
    private String testName;
    @JsonProperty("isValid")
    private Boolean valid = true;
    @JsonProperty("description")
    private String description = "Passed";

    public boolean match(JsonNode node) {
        JsonNode isValid = node.get("value").get("IsValid").get(0);
        JsonNode description = node.get("value").get("Description").get(0);

        return (isValid.get("value").asBoolean() == getValid()) && (description.get("value").asText().equals(getDescription()));
    }
}
