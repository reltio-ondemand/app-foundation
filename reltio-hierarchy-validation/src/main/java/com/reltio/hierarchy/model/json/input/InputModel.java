
package com.reltio.hierarchy.model.json.input;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputModel {
    @JsonProperty("envUrl")
    private String envUrl;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("entityUri")
    private List<String> entityUri;
    @JsonProperty("graphTypeUri")
    private List<String> graphTypeUri;
    @JsonProperty("entityAttributeUri")
    private String entityAttributeUri;
    @JsonProperty("validations")
    private List<Validation> validations;
}
