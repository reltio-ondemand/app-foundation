package com.reltio.hierarchy.model.node;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

public class RootNode extends TreeNode {
    private List<ChildNode> children = new ArrayList<>();

    public RootNode(JsonNode treeNode) {
        super(treeNode.get("root"));
        validate();
        setLevel(0);
    }

    private void validate() {
        int totalChild = getRoot().get("total").asInt();
        if (totalChild == 0 || getRoot().get("children") == null) {
            throw new RuntimeException("This is not a tree");
        }
    }

    public JsonNode getRoot() {
        return getNode();
    }

    @Override
    public List<ChildNode> getChildren() {
        if (!children.isEmpty()) return children;

        JsonNode children = getRoot().get("children");
        for (JsonNode child : children) {
            ChildNode childNode = new ChildNode(child);
            childNode.setLevel(this.getLevel() + 1);
            this.children.add(childNode);

            if (childNode.getParentCount() > childNode.getRootCount()) {
                childNode.setRootCount(childNode.getParentCount());
            }
        }

        return this.children;
    }
}
