
package com.reltio.hierarchy.model.json.request.hvattribute.add;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Value {
    @JsonProperty("TestName")
    private List<TestName> testName = null;
    @JsonProperty("IsValid")
    private List<IsValid> isValid = null;
    @JsonProperty("Description")
    private List<Description> description = null;

}
