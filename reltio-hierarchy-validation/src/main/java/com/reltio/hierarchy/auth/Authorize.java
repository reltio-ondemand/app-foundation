package com.reltio.hierarchy.auth;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;

public class Authorize {
    private static final String AUTH_URL = "https://auth.reltio.com/oauth/token";
    private String username;
    private String password;
    private TokenGeneratorService tokenGeneratorService;

    public Authorize(String username, String password)
        throws APICallFailureException, GenericException {
        this.username = username;
        this.password = password;
        generateToken();
    }

    private Authorize() {}

    private void generateToken()
        throws APICallFailureException, GenericException {
        this.tokenGeneratorService = new TokenGeneratorServiceImpl(username, password, AUTH_URL);
    }

    public TokenGeneratorService getTokenGeneratorService()
        throws APICallFailureException, GenericException {
        if (tokenGeneratorService == null) generateToken();
        return tokenGeneratorService;
    }

    public String getNewToken()
        throws APICallFailureException, GenericException {
        generateToken();
        return tokenGeneratorService.getNewToken();
    }

    public String getToken()
        throws APICallFailureException, GenericException {
        return tokenGeneratorService.getToken();
    }

}
