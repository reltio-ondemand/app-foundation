# README #

Script reads from a csv file of Company Names in the second column because the file has stock tickers as the first column. 
Can change the index to 0 if the names are in the first column. 

This script is not efficient in terms of thousands or millions of API calls using the WikiSynonyms API. 
However, my algorithm of cleaning the company names is ok to use in the future to clean some data. 

To run: python getSynonyms.py [input csv file of names in column 1 (0-based indexing)] [json output file name]

Example run: python getSynonyms.py companylist.csv outputSynonyms.json


