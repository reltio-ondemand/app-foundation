import requests
import json
import re
import itertools
import sys, csv, string

# To run: python getSynonyms.py [input csv file of names in column 1 (0-based indexing)] [x.json output file]
# Ex: python getSynonyms.py companyList.csv outputSynonyms.json

# Object for Company
class Company:
	# Method to store company names within Company object
	def __init__(self, in_name):
		self.name = in_name

# This allows you to switch from the default ASCII to other encodings such as UTF-8, 
# which the Python runtime will use whenever it has to decode a string buffer to unicode.
# It is recommended to use codecs module in python3, NOT reload and sys.setdefaultencoding.
reload(sys)
sys.setdefaultencoding('utf-8')

# List of structures in company names to remove from the name to obtain synonyms
structures = ['inc', 'corp', 'lp', 'corporation', 'company', 'incorporated', 'plc', 'sa', 'nv',
'ltd', 'limited', 'llc', 'sl', 'Inc', 'Corp', 'Corporation', 'Co', 'Company', 'Incorporated', 'PLC',
'Limited', 'Ltd', 'LLC', 'SL', 'LP', 'PLC', 'SA', 'NV']

# method to add new record to dictionary
# key is the search term
# val is the array of synonyms to store into
def addToDict(dictionary, key, val):
	dictionary[key] = val
	return dictionary

# method to get initials of key with spaces
'''
def getKeyInitialsWithSpaces(key):
	retVal = ''
	data = keyTokens.split()
	for i in range(len(data)):
		retVal += ''.join(data[i][0])
	return retVal
'''

# method to get the key/search term
def getKey(URL):
	# replace beginning of WikiSynonyms API URL with nothing (i.e. remove everything except key/search term)
	matchObj = re.sub(r'http://wikisynonyms.ipeirotis.com/api/', '', URL)
	# replace %20 with actual spaces
	matchObj = re.sub(r'%20', ' ', matchObj)
	return matchObj


# method to create URLs for WikiSynonyms API
# namesList is the list of company names/search terms
def createURLs(namesList):
	urlList = []
	# Set up the urlList for multiple WikiSynonyms API
	# Store beginning URL for WikiSynonyms API
	url = 'http://wikisynonyms.ipeirotis.com/api/'
	# Loop through namesList
	for i in range(len(namesList)):
		# add search term from namesList to beginning URL for WikiSynonyms API 
		url = url + namesList[i].name

		# if there is a space in the url
		if ' ' in url:
			# convert spaces to %20 encoding for space in URL
			url = re.sub(r' ', '%20', url)

		# add the urls to the urlList
		urlList.append(url)

		# reset the url to beginning URL for WikiSynonyms API
		url = 'http://wikisynonyms.ipeirotis.com/api/'

	# return the urlList once set up for all URL's are done
	return urlList	

# method to get all related terms of the key/search term
# arr is the array of names to search for
# key is the search term
def getRelated(arr, key):
	output = []
	# split key/search term into words and make them lowercase
	keyTokens = key.lower().split()
	# check if keyTokens is in related terms
	# Loop through arr (the array of synonyms from the WikiSynonyms API)
	for i in range(len(arr)):
		# store current name into variable tokens
		tokens = arr[i]
		# split the current name by spaces to separate each word in name
		tokens = tokens.lower().split()
		# store length of key/search term array keyTokens
		length = len(keyTokens)
		# check if keyTokens is in the current synonym name's tokens
		if set(keyTokens).issubset(tokens) == True:
			# if it is, add to the output array
			output.append(arr[i])

	# check if key initials in related terms
	keyInitials = []
	for i in range(len(keyTokens)):
		# if the length of the number of words in the key is more than 1, get the initials of each word and join them
		# otherwise, it is just one word, so add the first letter to the keyInitials list
		if len(keyTokens) > 1:
			keys = keyTokens[i].lower().split()
			keys = [keys[a][0] for a in range(len(keys))]
			keyInitials.append(''.join(keys))
		else:
			keyInitials.append(keyTokens[i])
	
	# join the keyInitials into a string of initials without spaces
	s = ''.join(keyInitials)

	# check if key initials without structure type in related terms
	# remove punctuation from keyTokens
	noPuncKeyTokens = [val.translate(None, string.punctuation) for val in keyTokens]
	for i in range(len(structures)):
		# if there is a structure type (i.e. incorporated) in noPuncKeyTokens, remove it
		if structures[i] in noPuncKeyTokens:
			noPuncKeyTokens.remove(structures[i])

	keyInitials = []
	# Then check the initials in related terms as above 
	for i in range(len(noPuncKeyTokens)):
		# if the length of the number of words in the key is more than 1, get the initials of each word and join them
		if len(noPuncKeyTokens) > 1:
			keys = noPuncKeyTokens[i].lower().split()
			keys = [keys[a][0] for a in range(len(keys))]
			keyInitials.append(''.join(keys))
	s2 = ''.join(keyInitials)

	for i in range(len(arr)):
		# split the key/search term into words
		resultWords = arr[i].lower().split()
		# check if the length of the string initials with no punctuation and no structure is greater than 1
		if len(s2) > 1:
			# check if the initials stored in 's' and 's2' are in the key/search term's words
			if s in resultWords or s2 in resultWords:
				# if so, add to the output list
				output.append(arr[i])

	# check if key initials in related term initials
	tokenInitials = []
	# get initials of each token and join them into a list
	for i in range(len(arr)):
		tokens = arr[i].lower().split()
		tokens = [tokens[a][0] for a in range(len(tokens))]
		tokenInitials.append(''.join(tokens))
	
	# fix the matching by initials
	indices = []
	for i in range(len(tokenInitials)):
		# if the string with the keyInitials is in the tokenInitials list, keep track of the index where a match is found
		if s in tokenInitials[i]:
			indices.append(i)

	# Match the index of the tokenInitials to the corresponding company
	for i in range(len(indices)):
		output.append(arr[indices[i]])

	# remove punctuation from search term and check if they are substrings of each other
	wholeName = ' '.join(keyTokens)
	wholeName = wholeName.translate(None, string.punctuation)
	#wholeName = wholeName.rsplit(' ', 1)[0]
	# remove spaces
	wholeName = re.sub(r' ', '', wholeName)

	# Go through all keys/search terms
	for i in range(len(arr)):
		# convert to lowercase
		nameToComp = str(arr[i].lower())
		# remove punctuation
		nameToComp = nameToComp.translate(None, string.punctuation)
		# remove spaces in nameToComp
		nameToComp = re.sub(r' ', '', nameToComp)
		# check if nameToComp starts with the whole search term or whole search term is in nameToComp or nameToComp is in wholeName
		# Examples: IBM and IBM Hong Kong, International Business Machines Corporation or International Business Machines 
		if nameToComp.startswith(wholeName) or wholeName in nameToComp or nameToComp in wholeName:
			output.append(arr[i])

	retArr = []
	for i in range(len(output)):
		# if WikiSynonyms API returns synonyms with 'List of' or 'Criticism of' in the name, remove them
		if 'List of' not in output[i] and 'Criticism of' not in output[i]:
			# add to the array
			retArr.append(output[i])

	# Make sure there are no repeating synonyms and return the set as a list (i.e. IBM and IBM)
	return list(set(retArr))


# dictionary to store data
dataDict = {}
# array to store synonym terms
arrTerms = []
finalTerms = []
val = []
# check for ValueError and skip name
namesList = []
urlList = []
# flag to make sure to add to the dictionary during the correct case
flag = 0
# get csv fileName to read as second commandline argument
fileName = sys.argv[1]

# open the given csv file
with open(fileName) as file:
	# use Python's csv reader
	reader = csv.reader(file)
	for row in reader:
		# get the company names from the second column of the given csv file
		name = row[1]
		# add the name to the Company object to the namesList
		namesList.append(Company(name))

# make a list of all the URL's
urlList = createURLs(namesList)
for a in range(len(urlList)):
	# Get the current URL
	url = urlList[a]
	try:
		# get response from WikiSynonyms API
		response = requests.get(url)
		# get JSON response
		json_data = json.loads(response.text)
		# parse the JSON response returned, only get the term value
		for i in json_data:
			if i != 'http' and i != 'message':
				for j in json_data[i]:
					arrTerms.append(j['term'])
	# if there are not results returned from the WikiSynonyms API
	except ValueError:
		# remove structures, or words with parentheses and add to dict
		# set flag to make sure not to add to the dictionary multiple times
		flag = 1
		print a, 'valueerror'

		# store the current name
		name = namesList[a].name 
		# store the current name as the key
		key = name
		# Remove periods from name
		name = re.sub(r'\.', '', name)
		#name = re.sub(r'\s\(*\)\s', '', name)
		# split the name by space into words
		nameTokens = name.split()
		for b in range(len(structures)):
			remStr = structures[b]
			# check if there is a structure in the name, if there is remove it from the tokens
			if remStr in nameTokens:
				nameTokens.remove(remStr)
		# join the tokens of the name with a space in between the words
		val = ' '.join(nameTokens)
		# remove the commas from the string
		val = re.sub(r',', '', val)
		# convert string into list
		val = [val]
		# add the key and value to the data dictionary
		dataDict = addToDict(dataDict, key, val)
		# reset the value list for the next value error
		val = []

	# if the flag is 0, a response from the WikiSynonyms API was returned
	if flag == 0:
		# get the key from the URL
		key = getKey(url)
		# get the related terms for the key
		val = getRelated(arrTerms, key)
		# add the keys and values to the data dictionary
		dataDict = addToDict(dataDict, key, val)
		# reset the arrTerms list
		arrTerms = []
	# reset the flag to 0 to repeat the process for the next term
	flag = 0

# write to an outputfile of the third command line argument
outputFile = sys.argv[2]
# open the outputfile for writing
with open(outputFile, 'w') as outfile:
	# write the data dictionary to a JSON format
	json.dump(dataDict, outfile, sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False)
#print json.dumps(dataDict, indent=4)
# print the length of the data dictionary
print len(dataDict)







