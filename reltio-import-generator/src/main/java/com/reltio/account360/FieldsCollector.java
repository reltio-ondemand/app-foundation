package com.reltio.account360;

import com.reltio.account360.config.Configuration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by apavlov on 17.07.2017.
 */
@Slf4j
@Getter
@RequiredArgsConstructor
public class FieldsCollector implements Observer {

    /**
     * collection depth means how many path's elements will be included into name of attribute
     * useful for inlined nested attributes
     */
    private int collectDepth = 1;

    /**
     * collect count means how many input fields will be generated for each attribute
     */
    private int collectCount = 1;

    private static String CLEANSER_TEMPLATE = "$[?((key == '%s' && value[%d].value == null) )]";

    private final Configuration config;

    @RequiredArgsConstructor
    @Getter
    public static class Consumer {
        private final String uri;
        private List<String> attributes = new LinkedList<>();
        private List<String> targetPaths = new LinkedList<>();
        //private List<String> cleanserExpressions = new LinkedList<>();
        private List<Reference> references = new LinkedList<>();

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(uri).append("\n");
            for(final String a: attributes) {
                sb.append(a).append("\n");
            }

            for(final Reference r: references) {
                sb.append("ref: ").append(r).append("\n");
            }

            return sb.toString();
        }
    }

    @AllArgsConstructor
    @Getter
    @ToString
    public static class Reference {
        private final String origin;
        private final String destination;
        private final String uri;
    }

    private final LinkedList<Consumer> consumers = new LinkedList<>();
    private List<Consumer> finished = new LinkedList<>();

    public void restart() {
        finished = new LinkedList<>();
    }

    @Override
    public void begin(final String uri) {
        // inline all nested attributes when we have two levels above
        Utils.AttrOption opt = Utils.getAttributeOption(uri, consumers.size(), config);
        collectCount = opt.getCount();
        if (!opt.isInlined()) consumers.add(new Consumer(uri));
        else collectDepth++;
    }

    @Override
    public void end() {
        assert !consumers.isEmpty();
        assert collectDepth >= 1;
        if (collectDepth != 1) {
            collectDepth--;
        } else finished.add(consumers.pollLast());
    }

    @Override
    public void attribute(final String uri) {
        assert !consumers.isEmpty();

        String name = Utils.uri2Name(uri, collectDepth, config.getCompositeNameDelimeter()!=null?config.getCompositeNameDelimeter():".");
        consumers.peekLast().getAttributes().add(name);
        consumers.peekLast().getTargetPaths().add(Utils.uri2Mapping(uri, 0));
        //consumers.peekLast().getCleanserExpressions().add(String.format(CLEANSER_TEMPLATE, name, 0));

        if (collectDepth > 1) {
            for (int i = 1; i < collectCount; ++i) {
                consumers.peekLast().getAttributes().add(name + "_" + i);
                consumers.peekLast().getTargetPaths().add(Utils.uri2Mapping(uri, i));
                //consumers.peekLast().getCleanserExpressions().add(String.format(CLEANSER_TEMPLATE, name, i));
            }
        }

        assert consumers.peekLast().getAttributes().size() == consumers.peekLast().getTargetPaths().size();
    }

    @Override
    public void reference(String uri, String referencedEntityTypeURI, String relationshipTypeURI) {
        assert !consumers.isEmpty();
        consumers.peekLast().getReferences().add(new Reference(uri, referencedEntityTypeURI, relationshipTypeURI));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("== start ==\n");
        for(Consumer c: finished) {
            sb.append(c);
        }

        sb.append("== end ==\n");
        return sb.toString();
    }

}
