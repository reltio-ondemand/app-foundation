package com.reltio.account360;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Created by apavlov on 18.07.2017.
 */
@AllArgsConstructor
@Slf4j
public class Traverser {
    private final List<Observer> observers;
    private final JsonArray entityTypes;

    public void traverse(final JsonObject object, String relocUri) {
        assert object != null;
        JsonPrimitive isAbstract = object.getAsJsonPrimitive("abstract");
        if (isAbstract != null && isAbstract.getAsBoolean()) return;
        String uri = (relocUri!=null)?relocUri:object.getAsJsonPrimitive("uri").getAsString();
        assert uri != null;
        observers.stream().forEach(x -> x.begin(uri));
        JsonArray attributes = object.getAsJsonArray("attributes");
        if (attributes != null) {
            for(JsonElement e: attributes) {
                JsonObject o = e.getAsJsonObject();
                JsonPrimitive type = o.getAsJsonPrimitive("type");
                JsonPrimitive attrUri = o.getAsJsonPrimitive("uri");
                JsonPrimitive attrName = o.getAsJsonPrimitive("name");

                if (attrUri == null) {
                    log.warn("empty uri in attribute {}", o);
                    continue;
                }

                if (attrName == null) {
                    log.warn("Name of attribute {} is empty", attrUri);
                }

                String strType = (type==null)?(o.getAsJsonArray("attributes")!=null?"Nested":"String"):type.getAsString();

                if (strType.equals("Nested")) {
                    traverse(o, null);
                } else if (strType.equals("Reference")) {
                    JsonPrimitive referencedEntityTypeURI = o.getAsJsonPrimitive("referencedEntityTypeURI");
                    JsonPrimitive relationshipTypeURI = o.getAsJsonPrimitive("relationshipTypeURI");
                    for (JsonElement je: entityTypes) {
                        JsonObject jo = je.getAsJsonObject();
                        String joUri = jo.getAsJsonPrimitive("uri").getAsString();
                        // TODO - remove this hack
                        if (jo.getAsJsonPrimitive("uri").getAsString().equals(referencedEntityTypeURI.getAsString()))/* ||
                                jo.getAsJsonPrimitive("uri").getAsString().equals(attrUri.getAsString()) ||
                                jo.getAsJsonPrimitive("uri").getAsString().equals("configuration/entityTypes/HCP/attributes/Address") ||
                                jo.getAsJsonPrimitive("uri").getAsString().equals("configuration/entityTypes/HCP/attributes/Employment"))*/ {
                            // parse reference as nested attribute
                            //log.info("parse reference {} as nested", joUri);
                            //jo.addProperty("uri", attrUri.getAsString());
                            //log.info("relocate reference as {}", attrUri.getAsString());
                            traverse(jo, attrUri.getAsString());
                            break;
                        }
                    }

                    //if (referencedEntityTypeURI == null || relationshipTypeURI == null) {
                    //    log.warn("reference {} doesn't contain referencedEntityTypeURI or relationshipTypeURI", attrUri.getAsString());
                    //    continue;
                    //}

                    //observers.stream().forEach(x -> x.reference(attrUri.getAsString(), referencedEntityTypeURI.getAsString(), relationshipTypeURI.getAsString()));
                    //log.info("skip Reference attribute now {}", attrUri);
                } else {
                    observers.stream().forEach(x -> x.attribute(attrUri.getAsString()));
                }
            }
        }

        observers.stream().forEach(x -> x.end());
    }
}
