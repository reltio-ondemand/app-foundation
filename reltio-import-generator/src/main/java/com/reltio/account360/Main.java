package com.reltio.account360;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.reltio.account360.config.ConfigurationProvider;
import com.reltio.account360.config.ResourcesConfigProvider;
import com.reltio.account360.snaplogic.Pipeline;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
public class Main {

    private static final int ROWS_COUNT = 100000;

    public static void main(final String[] args) throws IOException {
        log.info("started...");

        if (args.length < 2) {
            log.warn("Please provide tenant configuration in first argument and destination directory in second");
            return;
        }

        Path tenantConfigPath = Paths.get(args[0]);
        String tenant = tenantConfigPath.getFileName().toString();
        String tenantName = tenant.substring(0, (tenant.lastIndexOf(".")!=-1?tenant.lastIndexOf("."):tenant.length()));

        List<Observer> observers = new LinkedList<>();
        ConfigurationProvider cp = new ResourcesConfigProvider();
        observers.add(new FieldsCollector(cp.getConfiguration()));
        try (BufferedReader streamReader = new BufferedReader(new FileReader(args[0]))) {
            //BufferedReader streamReader = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
            StringBuilder builder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                builder.append(inputStr);

            JsonParser parser = new JsonParser();
            JsonObject gson = parser.parse(builder.toString()).getAsJsonObject();

            JsonArray entities = gson.getAsJsonArray("entityTypes");
            Traverser t = new Traverser(observers, entities);
            Map<String, List<FieldsCollector.Consumer>> result = new HashMap<>();
            if (entities != null) {
                for (final JsonElement e : entities) {
                    JsonObject o = e.getAsJsonObject();
                    assert o != null;
                    t.traverse(o.getAsJsonObject(), null);
                    for(Observer obs: observers) {
                        FieldsCollector fc = (FieldsCollector) obs;
                        String name = Utils.uri2Name(o.getAsJsonPrimitive("uri").getAsString(), 1, ".");
                        result.put(name, fc.getFinished());
                        fc.restart();
                        log.info("Collected {} results {}", name, result.get(name).size());
                    }
                }
            }

            File tenantDir = Paths.get(args[1], tenantName).toFile();

            if (!tenantDir.exists()) {
                tenantDir.mkdir();
            }

            List<String> itemsHCP = new ArrayList<>();
            List<String> itemsHCO = new ArrayList<>();

            for(Map.Entry<String, List<FieldsCollector.Consumer>> entry: result.entrySet()) {
                Path p = Paths.get(args[1], tenantName, entry.getKey());
                File dir = p.toFile();
                if (!dir.exists()) {
                    dir.mkdir();
                }

                for(final FieldsCollector.Consumer c: entry.getValue()) {
                    int inc = 0;
                    if (entry.getKey().equals("HCO")) inc = ROWS_COUNT;
                    String name = Utils.uri2Name(c.getUri(), 1, "");
                    Path slpPath = p.resolve(entry.getKey() + "_" + name + ".slp");
                    Path csvPath = p.resolve("LSC_" + name + ".csv");
                    Path csvTestPath = p.resolve(entry.getKey() + "_" + name + ".csv");
                    StringBuilder sb = new StringBuilder();
                    StringBuilder csvHeader = new StringBuilder();
                    Pipeline pp = new Pipeline();
                    Iterator<String> srcItr = c.getAttributes().iterator();
                    Iterator<String> targetItr = c.getTargetPaths().iterator();
                    sb.append("SrcId|SrcCd|");
                    while (srcItr.hasNext()) {
                        String src = srcItr.next();
                        pp.addTransformation("$['" + src + "']", true, "$" + targetItr.next());
                        sb.append(src);
                        csvHeader.append(src);
                        if (srcItr.hasNext()) {
                            sb.append("|");
                        }
                        csvHeader.append(",");
                    }

                    // inject type of entity
                    //pp.addTransformation(c.getUri(), false, "$type");
                    pp.addTransformation("$SrcId+$SrcCd", true, "$SRC_ID");
                    pp.addTransformation("$SrcId", true, "crosswalks[0].value");
                    pp.addTransformation("$SrcCd", true, "crosswalks[0].type");
                    csvHeader.append("SrcId,SrcCd");
                    pp.setTransformatorLabel(name);
                    pp.setCleanerLabel("Nulls cleanser");
                    pp.setLabel(name);

                    try(  PrintWriter out = new PrintWriter( slpPath.toFile() )  ){
                        out.println( pp.toString() );
                    }

                    try(  PrintWriter out = new PrintWriter( csvPath.toFile() )  ){
                        out.println( sb.toString() );
                    }


                    // generate test data files
                    Random rnd = new Random();
                    try(  PrintWriter out = new PrintWriter( csvTestPath.toFile() )  ){
                        out.println( csvHeader.toString() );
                        StringBuilder staff = new StringBuilder();
                        // generate test data
                        for(int i = 0; i < ROWS_COUNT; ++i) {
                            Iterator<String> fieldsItr = c.getAttributes().iterator();
                            while(fieldsItr.hasNext()) {
                                String field = fieldsItr.next();
                                if (rnd.nextBoolean() && !field.contains("Date") && !field.contains("Year") && !field.contains("GPA")) {
                                    staff.append(Utils.generateRandomWords(rnd)).append(",");
                                } else {
                                    staff.append(",");
                                }
                            }

                            Integer id = rnd.nextInt(ROWS_COUNT) + inc;

                            if (entry.getKey().equals("HCP")) {
                                itemsHCP.add(id.toString());
                            } else if (entry.getKey().equals("HCO")) {
                                itemsHCO.add(id.toString());
                            }

                            staff.append(id)
                                    .append(",")
                                    .append("configuration/sources/HMS");
                            out.println(staff.toString());
                            staff.setLength(0);
                        }
                    }
                }
            }

            Path hhPath = Paths.get(args[1], tenantName);
            Path csvPath = hhPath.resolve("LSC_LSCO.csv");
            Path csvTestPath = hhPath.resolve("HCP_HCO.csv");
            Random rnd = new Random();
            try(  PrintWriter out = new PrintWriter( csvPath.toFile() )  ){
                out.println( "HCPId|HCPCd|HCOId|HCOCd" );
            }

            try(  PrintWriter out = new PrintWriter( csvTestPath.toFile() )  ){
                out.println( "HCPId,HCPCd,HCOId,HCOCd"  );
                StringBuilder staff = new StringBuilder();
                for(int i = 0; i < ROWS_COUNT; ++i) {
                    String idHCP = itemsHCP.get(rnd.nextInt(itemsHCP.size()));
                    String idHCO = itemsHCO.get(rnd.nextInt(itemsHCO.size()));

                    staff.append(idHCP)
                            .append(",configuration/sources/HMS,")
                            .append(idHCO)
                            .append(",configuration/sources/HMS");
                    out.println(staff.toString());
                    staff.setLength(0);
                }
            }

            /*
            // prepare pipelines
            for(Observer o: observers) {
                assert ((FieldsCollector)o).getConsumers().isEmpty();
                FieldsCollector fc = (FieldsCollector)o;
                for(FieldsCollector.Consumer c: fc.getFinished()) {
                    Pipeline pp = new Pipeline();
                    Iterator<String> srcItr = c.getAttributes().iterator();
                    Iterator<String> targetItr = c.getTargetPaths().iterator();
                    while(srcItr.hasNext()) {
                        pp.addTransformation("$" + srcItr.next(), true, "$" + targetItr.next());
                    }

                    // inject type of entity
                    pp.addTransformation(c.getUri(), false, "$type");
                    pp.setTransformatorLabel(Utils.uri2Name(c.getUri(),1 ,""));
                    pp.setCleanerLabel("Nulls cleanser");
                    //log.info("pipeline: {}", pp);
                }
                log.info("{}", o);
            }
            */

            log.info("scan relation section");
            observers.clear();
            observers.add(new FieldsCollector(cp.getConfiguration()));
            Traverser t2 = new Traverser(observers, entities);
            entities = gson.getAsJsonArray("relationTypes");
            if (entities != null) {
                for (final JsonElement e : entities) {
                    JsonObject o = e.getAsJsonObject();
                    assert o != null;
                    t2.traverse(o.getAsJsonObject(), null);
                }
            }

            //for(Observer o: observers) {
            //    assert ((FieldsCollector)o).getConsumers().isEmpty();
            //    log.info("{}", o);
            //}


        } catch(IOException e) {
            log.error("I/O exception {}", e.getMessage());
        }
    }
}