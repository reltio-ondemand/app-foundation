package com.reltio.account360;

import com.reltio.account360.config.AttributeOptions;
import com.reltio.account360.config.Configuration;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Random;

/**
 * Created by apavlov on 17.07.2017.
 */
public class Utils {

    @AllArgsConstructor
    @Getter
    public static final class AttrOption {
        private final int count;
        private final boolean inlined;
    }

    public static String uri2Name(String uri, int collectedCount, final String delimeter) {
        String[] items = uri.split("\\/");
        // TODO check items count here!
        // TODO for collectedCount == 1
        StringBuilder sb = new StringBuilder();
        for(int i = collectedCount - 1; i >= 0; --i) {
            sb.append(items[items.length - i*2 - 1]);
            if (i != 0) sb.append(delimeter);
        }

        return sb.toString();
    }

    public static String uri2Mapping(final String uri, int position) {
        String[] items = uri.split("\\/");
        StringBuilder sb = new StringBuilder();
        boolean started = false;
        for(final String s: items) {
            if (s.equals("attributes")) {
                sb.append(started?".value":s);
                started = true;
            } else {
                if (started) {
                    sb.append(".")
                            .append(s)
                            .append("[" + position + "]");
                    position = 0;
                }
            }
        }

        if (sb.length() != 0) sb.append(".value");

        return sb.toString();
    }


    public static AttrOption getAttributeOption(final String uri, int levelsAbove, Configuration config) {
        // all attributes on level two and deeper should be inlined
        if (levelsAbove >= 2) return new AttrOption(config.getDefaultInlineCount()!=null?config.getDefaultInlineCount():1
                , true);

        if (config.getAttributeOptions() != null) {
            for(AttributeOptions ini: config.getAttributeOptions()) {
                if (ini.getUri().equals(uri)) return new AttrOption(ini.getCount(), ini.getInline());
            }
        }

        // return default values
        return new AttrOption(config.getDefaultInlineCount()!=null?config.getDefaultInlineCount():1
                , config.getDefaultInlineNested()!=null?config.getDefaultInlineNested():false);
    }

    public static String generateRandomWords(final Random random) {
        char[] word = new char[random.nextInt(8)+3]; // words of length 3 through 10. (1 and 2 letter words are boring.)
        for(int j = 0; j < word.length; j++) {
                word[j] = (char)('a' + random.nextInt(26));
        }

        return new String(word);
    }
}
