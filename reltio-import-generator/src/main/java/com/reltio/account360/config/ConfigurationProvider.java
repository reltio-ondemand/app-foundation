package com.reltio.account360.config;

/**
 * Created by apavlov on 18.07.2017.
 */
public interface ConfigurationProvider {
    Configuration getConfiguration();
}
