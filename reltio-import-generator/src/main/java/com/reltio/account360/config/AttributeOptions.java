package com.reltio.account360.config;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by apavlov on 18.07.2017.
 */
@Data
@EqualsAndHashCode(exclude={"count", "inline"})
public class AttributeOptions {
    private Integer count;
    private String uri;
    private Boolean inline;
}
