package com.reltio.account360.config;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

/**
 * Created by apavlov on 18.07.2017.
 */
public class ResourcesConfigProvider implements ConfigurationProvider {

    private static final Type CFG_TYPE = new TypeToken<Configuration>() {
    }.getType();

    @Override
    public Configuration getConfiguration() {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream( "config.json");
        assert is != null;
        JsonReader reader = new JsonReader(new InputStreamReader(is));
        return new Gson().fromJson(reader, CFG_TYPE);
    }
}
