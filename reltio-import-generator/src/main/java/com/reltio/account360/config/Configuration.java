package com.reltio.account360.config;

import lombok.Data;

import java.util.List;

/**
 * Created by apavlov on 18.07.2017.
 */
@Data
public class Configuration {
    private String compositeNameDelimeter;
    private Boolean defaultInlineNested;
    private Integer defaultInlineCount;
    private List<AttributeOptions> attributeOptions;
}
