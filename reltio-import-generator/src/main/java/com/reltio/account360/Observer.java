package com.reltio.account360;

/**
 * Created by apavlov on 17.07.2017.
 */
public interface Observer {

    void begin(final String uri);
    void end();
    void attribute(final String uri);
    void reference(final String uri
            , final String referencedEntityTypeURI
            , final String relationshipTypeURI);
}
