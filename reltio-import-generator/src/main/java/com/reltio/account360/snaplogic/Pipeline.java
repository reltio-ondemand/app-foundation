package com.reltio.account360.snaplogic;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by apavlov on 19.07.2017.
 */
public class Pipeline {
    JsonElement root;
    JsonObject transformator;
    JsonArray transformationMap;
    JsonObject properties;

    JsonObject cleaner;

    public Pipeline() {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream( "template_1.json");
        assert is != null;
        JsonReader reader = new JsonReader(new InputStreamReader(is));
        JsonParser parser = new JsonParser();
        root = parser.parse(reader);
        assert root != null;
        transformator = root.getAsJsonObject().getAsJsonObject("snap_map").getAsJsonObject("transformator");
        assert transformator != null;
        transformationMap = transformator
                .getAsJsonObject("property_map")
                .getAsJsonObject("settings")
                .getAsJsonObject("transformations")
                .getAsJsonObject("value")
                .getAsJsonObject("mappingTable")
                .getAsJsonArray("value");
        assert transformationMap != null;

        cleaner = root.getAsJsonObject().getAsJsonObject("snap_map").getAsJsonObject("cleaner");
        assert cleaner != null;
        properties = root.getAsJsonObject().getAsJsonObject("property_map");
        assert properties != null;
    }

    public void addTransformation(final String expression, boolean isExpression, final String targetPath) {
        assert expression != null && !expression.isEmpty();
        Gson gson = new Gson();
        transformationMap.add(gson.toJsonTree(new MappingElement(new Expression(isExpression, expression), new Expression(null, targetPath))));
    }

    public void setTransformatorLabel(final String label) {
        transformator.getAsJsonObject("property_map")
                .getAsJsonObject("info")
                .getAsJsonObject("label")
                .addProperty("value", label);
    }

    public void setCleanerLabel(final String label) {
        cleaner.getAsJsonObject("property_map")
                .getAsJsonObject("info")
                .getAsJsonObject("label")
                .addProperty("value", label);
    }

    public void setLabel(final String label) {
        properties.getAsJsonObject("info")
                .getAsJsonObject("label")
                .addProperty("value", label);
    }

    @Override
    public String toString() {
        return root.toString();
    }

}
