package com.reltio.account360.snaplogic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by apavlov on 19.07.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MappingElement {
    private Expression expression;
    private Expression targetPath;
}
