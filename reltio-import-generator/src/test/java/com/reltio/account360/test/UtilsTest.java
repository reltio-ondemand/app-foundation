package com.reltio.account360.test;


import com.reltio.account360.Utils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testUri2Name() {
        assertEquals("TransferDUNSNumber", Utils.uri2Name("configuration/entityTypes/Organization/attributes/PriorDUNSNUmber/attributes/TransferDUNSNumber", 1, "."));
        assertEquals("PriorDUNSNUmber.TransferDUNSNumber", Utils.uri2Name("configuration/entityTypes/Organization/attributes/PriorDUNSNUmber/attributes/TransferDUNSNumber", 2, "."));
        assertEquals("Organization.PriorDUNSNUmber.TransferDUNSNumber", Utils.uri2Name("configuration/entityTypes/Organization/attributes/PriorDUNSNUmber/attributes/TransferDUNSNumber", 3, "."));
    }

    @Test
    public void testUri2Mapping() {
        assertEquals("attributes.PriorDUNSNUmber[0].value.TransferDUNSNumber[0].value", Utils.uri2Mapping("configuration/entityTypes/Organization/attributes/PriorDUNSNUmber/attributes/TransferDUNSNumber", 0));
        assertEquals("attributes.PriorDUNSNUmber[2].value.TransferDUNSNumber[0].value", Utils.uri2Mapping("configuration/entityTypes/Organization/attributes/PriorDUNSNUmber/attributes/TransferDUNSNumber", 2));
        assertEquals("attributes.PriorDUNSNUmber[10].value.TransferDUNSNumber[0].value.SomeData[0].value", Utils.uri2Mapping("configuration/entityTypes/Organization/attributes/PriorDUNSNUmber/attributes/TransferDUNSNumber/attributes/SomeData", 10));
        assertEquals( "attributes.ISO3166-2[1].value", Utils.uri2Mapping("configuration/entityTypes/Location/attributes/ISO3166-2", 1));
        assertEquals( "", Utils.uri2Mapping("configuration/entityTypes/Location//ISO3166-2", 1));
    }
}