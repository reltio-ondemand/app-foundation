package com.reltio.account360.test;

import com.reltio.account360.config.Configuration;
import com.reltio.account360.config.ConfigurationProvider;
import com.reltio.account360.config.ResourcesConfigProvider;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by apavlov on 18.07.2017.
 */
public class ResourcesConfigProviderTest {

    @Test
    public void testResourceConfig() {
        ConfigurationProvider cp = new ResourcesConfigProvider();
        Configuration cfg = cp.getConfiguration();
        assertTrue(cfg != null);
        assertEquals(false, cfg.getDefaultInlineNested());
        assertEquals(".", cfg.getCompositeNameDelimeter());
        assertEquals(7, cfg.getAttributeOptions().size());
    }
}
