package com.reltio.account360.test;

import com.reltio.account360.snaplogic.Pipeline;
import org.junit.Test;

/**
 * Created by apavlov on 19.07.2017.
 */
public class PipelineTest {

    @Test
    public void testPipelineSimple() {
        Pipeline pp = new Pipeline();
        pp.setTransformatorLabel("Test");
        pp.addTransformation("CSV_FIELD", true, "attributes.Name[0].value");
        System.out.print(pp.toString());
    }
}
