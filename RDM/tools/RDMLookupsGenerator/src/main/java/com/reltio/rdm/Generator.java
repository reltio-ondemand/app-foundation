package com.reltio.rdm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.common.io.Files;
import com.reltio.rdm.model.LookupCode;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class Generator {
    private ObjectMapper mapper = new ObjectMapper();

    @Option(name = "-cs", aliases = {"--client.secret"}, usage = "Client Secret (e.g. /path/to/client_secret.json)", required = true)
    private String clientSecretFile;

    @Option(name = "-s", aliases = {"--sheet.id"}, usage = "Google Sheet Id (e.g. 1ZTeiwDGz5lLyUC2TiPxyMZCtlJkiGjF5a__AP5mRMg0)", required = true)
    private String sheetId;

    @Option(name = "-r", aliases = {"--sheet.range"}, usage = "Sheet range to be read (e.g. e.g. Sheet1!A2:B)", required = true)
    private String sheetRange;

    @Option(name = "-t", aliases = {"--template.json"}, usage = "RDM Lookup Template (e.g. /path/to/template.json)", required = true)
    private String templateFile;

    public void run() throws IOException {
        try {
            // authorize & get service interface
            Sheets service = SheetsUtil.getSheetsService(clientSecretFile);

            // read from the sheet
            ValueRange response = service.spreadsheets().values().get(sheetId, sheetRange).execute();
            List<List<Object>> values = response.getValues();

            // Get the template file as text
            String template = Files.toString(new File(templateFile), Charset.defaultCharset());

            List<LookupCode> lookupCodes = new ArrayList<>();
            for (List row : values) {
                String CODE = row.get(0).toString();
                String DESC = row.get(1).toString();

                String formatted = template.replaceAll("<CODE>", CODE).replaceAll("<DESC>", DESC);

                // objectify
                LookupCode lookupCode = mapper.readValue(formatted, LookupCode.class);
                lookupCodes.add(lookupCode);
            }

            File result = new File("lookups.json");
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.writeValue(result, lookupCodes);
            System.out.println("Find lookup-codes at: " + result.getAbsolutePath());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
