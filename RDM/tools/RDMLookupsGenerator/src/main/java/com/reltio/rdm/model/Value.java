
package com.reltio.rdm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "value",
    "enabled",
    "canonicalValue",
    "downStreamDefaultValue"
})
public class Value {

    @JsonProperty("code")
    private String code;
    @JsonProperty("value")
    private String value;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("canonicalValue")
    private Boolean canonicalValue;
    @JsonProperty("downStreamDefaultValue")
    private Boolean downStreamDefaultValue;

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("canonicalValue")
    public Boolean getCanonicalValue() {
        return canonicalValue;
    }

    @JsonProperty("canonicalValue")
    public void setCanonicalValue(Boolean canonicalValue) {
        this.canonicalValue = canonicalValue;
    }

    @JsonProperty("downStreamDefaultValue")
    public Boolean getDownStreamDefaultValue() {
        return downStreamDefaultValue;
    }

    @JsonProperty("downStreamDefaultValue")
    public void setDownStreamDefaultValue(Boolean downStreamDefaultValue) {
        this.downStreamDefaultValue = downStreamDefaultValue;
    }
}
