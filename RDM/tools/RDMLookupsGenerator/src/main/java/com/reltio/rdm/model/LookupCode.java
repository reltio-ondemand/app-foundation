
package com.reltio.rdm.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "tenantId",
    "code",
    "enabled",
    "sourceMappings"
})
public class LookupCode {

    @JsonProperty("type")
    private String type;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("code")
    private String code;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("sourceMappings")
    private List<SourceMapping> sourceMappings = null;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("tenantId")
    public String getTenantId() {
        return tenantId;
    }

    @JsonProperty("tenantId")
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("sourceMappings")
    public List<SourceMapping> getSourceMappings() {
        return sourceMappings;
    }

    @JsonProperty("sourceMappings")
    public void setSourceMappings(List<SourceMapping> sourceMappings) {
        this.sourceMappings = sourceMappings;
    }
}
