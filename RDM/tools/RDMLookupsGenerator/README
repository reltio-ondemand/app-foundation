# RDM Lookups Generator

This program reads RDM Lookup data (Code, Description) from Google Sheets (e.g. https://goo.gl/QzPESD) using Sheets API and uses those data to replace
placeholder found in template json snippet.

## Step 1: Generate client_secret.json (e.g. /src/main/resources/client_secret.json)
Follow this <https://developers.google.com/identity/sign-in/web/devconsole-project?authuser=1> document to generate one.


## Step 2: Create template file like /src/main/resources/template.json.
Its content should resemble desired json object for the intended lookup code. Placeholders <CODE> & <DESC> should be
provided in proper place for lookup-code and lookup-description respectively.

For instance, use /src/main/resources/template.json as starting point and
a) Change value of "type" to the desired lookupType, e.g. "rdm/lookupTypes/MyLookUpType"
b) Update "sourceMappings" section, add new source objects as required.
c) Only one source should have "canonicalValue": true. Keep all other to false.


## Step 3: Build & Run

a) Build: mvn clean package
b) Run the jar rdm-lookup-gen-1.0-SNAPSHOT.jar:
java -Dmainclass=com.reltio.rdm.Generator -jar rdm-lookup-gen-1.0-SNAPSHOT.jar -cs </path/to/client_secret.json> -s <google-sheet-id> -r <sheet-range> -t /path/to/template.json

 -cs (--client.secret) : Client Secret (e.g. /path/to/client_secret.json)
 -s (--sheet.id)       : Google Sheet Id (e.g. 1ZTeiwDGz5lLyUC2TiPxyMZCtlJkiGjF5a__AP5mRMg0)
 -r (--sheet.range)    : Sheet range to be read (e.g. e.g. Sheet1!A2:B)
 -t (--template.json)  : RDM Lookup Template (e.g. /path/to/template.json)