import json

template = '''  {
    "tenantId": "{{rdmTenant}}",
    "type": "rdm/lookupTypes/OrganizationType",
    "code": "{{CODE}}",
    "enabled": true,
    "sourceMappings": [
        {
            "source": "Reltio",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": true,
                    "downStreamDefaultValue": true
                }
            ]
        },
        {
            "source": "ReltioRDM",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": false,
                    "downStreamDefaultValue": true
                }
            ]
        }
    ],
    "localizations": [],
    "parents": [],
    "startDate": 0,
    "endDate": 0
  }'''

in_filename = 'acc360_OrgType.json'
out_filename = 'OrganizationType.json'

# taken from https://gist.github.com/mshafrir/2646763
with open(in_filename) as f:
    codelist = json.load(f)

result = '[\n'
result += ',\n'.join(map(lambda code: template.replace('{{CODE}}', code['CODE']).replace('{{VALUE}}', code['VALUE']), codelist))
result += "\n]"

with open(out_filename, 'w') as fout:
    fout.write(result.encode('utf-8').strip())
