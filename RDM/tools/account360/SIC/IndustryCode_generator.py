import json
import csv

template = '''  {
    "tenantId": "{{rdmTenant}}",
    "type": "rdm/lookupTypes/App-Account360IndustryCode",
    "code": "{{CODE}}",
    "enabled": true,
    "sourceMappings": [
        {
            "source": "Reltio",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": true,
                    "downStreamDefaultValue": true
                }
            ]
        },
        {
            "source": "ReltioRDM",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": false,
                    "downStreamDefaultValue": true
                }
            ]
        }
    ],
    "localizations": [],
    "parents": [
    "rdm/lookupTypes/App-Account360IndustryType/SIC 1",
    "rdm/lookupTypes/App-Account360IndustryType/SIC 2",
    "rdm/lookupTypes/App-Account360IndustryType/SIC 3",
    "rdm/lookupTypes/App-Account360IndustryType/SIC 4",
    "rdm/lookupTypes/App-Account360IndustryType/SIC 5",
    "rdm/lookupTypes/App-Account360IndustryType/SIC 6",
    "rdm/lookupTypes/App-Account360IndustryType/US Standard Industry Code 1987 - 4 digit",
    "rdm/lookupTypes/App-Account360IndustryType/US SIC (1987)",
    "rdm/lookupTypes/App-Account360IndustryType/US Standard Industry Code 1972 - 4 digit",
    "rdm/lookupTypes/App-Account360IndustryType/US SIC (1972)",
    "rdm/lookupTypes/App-Account360IndustryType/US Standard Industry Code 1972 - 4 digit (European version)",
    "rdm/lookupTypes/App-Account360IndustryType/US Standard Industry Code 1987 - 3 digit",
    "rdm/lookupTypes/App-Account360IndustryType/US SIC (1987) - 3 digit",
    "rdm/lookupTypes/App-Account360IndustryType/US Standard Industry Code 1987 - 2 digit"
	],
    "startDate": 0,
    "endDate": 0
  }'''


def write_to_file(lookup_type, codelist, out_filename, parent_type):
    result = '[\n'
    result += ',\n'.join(map(lambda code: template
                             .replace('{{CODE}}', code[0])
                             .replace('{{VALUE}}', code[1])
                             .replace('{{lookupType}}', lookup_type)
                             .replace('{{parentCode}}', parent_type)
                             , codelist))
    result += "\n]"

    with open(out_filename, 'w') as fout:
        fout.write(result.encode('utf-8').strip())


def generate(lookup_type, filename):
    lookup_type = lookup_type
    in_filename = filename + '.csv'

    buf_limit = 100
    buf = []
    part = 0
    with open(in_filename) as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            buf.append(row)
            if len(buf) >= buf_limit:
                write_to_file(lookup_type, buf, '../../../../../app-account360/RDM/RdmLookups/App-Account360IndustryCode/App-Account360%s_%d-%d.json' %
                              (lookup_type, part * buf_limit + 1, part * buf_limit + len(buf)), filename)
                buf = []
                part += 1

    if buf:
        write_to_file(lookup_type, buf, '../../../../../app-account360/RDM/RdmLookups/App-Account360IndustryCode/App-Account360%s_%d-%d.json' %
                      (lookup_type, part * buf_limit + 1, part * buf_limit + len(buf)), filename)


generate("IndustryCodeSIC", "SIC")