import json
import csv

template = '''  {
    "tenantId": "{{rdmTenant}}",
    "type": "rdm/lookupTypes/Currency",
    "code": "{{CODE}}",
    "enabled": true,
    "sourceMappings": [
        {
            "source": "Reltio",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": true,
                    "downStreamDefaultValue": true
                }
            ]
        },
        {
            "source": "ReltioRDM",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": false,
                    "downStreamDefaultValue": true
                }
            ]
        }
    ],
    "localizations": [],
    "parents": [],
    "startDate": 0,
    "endDate": 0
  }'''


def write_to_file(lookup_type, codelist, out_filename, parent_type):
    result = '[\n'
    result += ',\n'.join(map(lambda code: template
                             .replace('{{CODE}}', code[0])
                             .replace('{{VALUE}}', code[1])
                             , codelist))
    result += "\n]"

    with open(out_filename, 'w') as fout:
        fout.write(result.strip())


def generate(lookup_type, filename):
    lookup_type = lookup_type
    in_filename = filename + '.csv'

    buf_limit = 100
    buf = []
    part = 0
    with open(in_filename) as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            buf.append(row)
            if len(buf) >= buf_limit:
                write_to_file(lookup_type, buf, '../../RdmLookups/Currency/LookupCodes_%s_%d-%d.json' %
                              (lookup_type, part * buf_limit + 1, part * buf_limit + len(buf)), filename)
                buf = []
                part += 1

    if buf:
        write_to_file(lookup_type, buf, '../../RdmLookups/Currency/LookupCodes_%s_%d-%d.json' %
                      (lookup_type, part * buf_limit + 1, part * buf_limit + len(buf)), filename)


generate("Currency", "currency")
