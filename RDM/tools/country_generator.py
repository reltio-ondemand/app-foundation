import json

template = '''  {
    "tenantId": "{{rdmTenant}}",
    "type": "rdm/lookupTypes/Country",
    "code": "{{CODE}}",
    "enabled": true,
    "sourceMappings": [
        {
            "source": "Reltio",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": true,
                    "downStreamDefaultValue": true
                }
            ]
        },
        {
            "source": "ReltioRDM",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": false,
                    "downStreamDefaultValue": true
                }
            ]
        }
    ],
    "localizations": [],
    "parents": [],
    "startDate": 0,
    "endDate": 0
  }'''

with open('iso3166-2.json') as f:
    codelist = json.load(f)

N = 100  # can add only 100 lookups in one call
codelist_parts = [codelist[i:i + N] for i in range(0, len(codelist), N)]

for i, codes in enumerate(codelist_parts):
    result = '[\n'
    result += ',\n'.join(
        map(lambda code: template.replace('{{CODE}}', code['Code']).replace('{{VALUE}}', code['Name']), codes))
    result += "\n]"

    with open('../RdmLookups/Country/LookupCodes_Country_%d-%d.json' % (100*i+1, 100*i + len(codes)), 'w') as fout:
        fout.write(result.encode('utf-8').strip())
