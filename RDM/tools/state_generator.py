import json

template = '''  {
    "tenantId": "{{rdmTenant}}",
    "type": "rdm/lookupTypes/State",
    "code": "{{CODE}}",
    "enabled": true,
    "sourceMappings": [
        {
            "source": "Reltio",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": true,
                    "downStreamDefaultValue": true
                }
            ]
        },
        {
            "source": "ReltioRDM",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": false,
                    "downStreamDefaultValue": true
                }
            ]
        }
    ],
    "localizations": [],
    "parents": ["rdm/lookupTypes/Country/DE"],
    "startDate": 0,
    "endDate": 0
  }'''

in_filename = 'de_states.json'
out_filename = 'de_state.json'

# taken from https://gist.github.com/mshafrir/2646763
with open(in_filename) as f:
    codelist = json.load(f)

result = '[\n'
result += ',\n'.join(map(lambda code: template.replace('{{CODE}}', code['abbreviation']).replace('{{VALUE}}', code['name']), codelist))
result += "\n]"

with open('../RdmLookups/State/' + out_filename, 'w') as fout:
    fout.write(result.encode('utf-8').strip())
