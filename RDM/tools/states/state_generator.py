import csv

template = '''  {
    "tenantId": "{{rdmTenant}}",
    "type": "rdm/lookupTypes/State",
    "code": "{{CODE}}",
    "enabled": true,
    "sourceMappings": [
        {
            "source": "Reltio",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": true,
                    "downStreamDefaultValue": true
                }
            ]
        },
        {
            "source": "ReltioRDM",
            "values": [
                {
                    "code": "{{CODE}}",
                    "value": "{{VALUE}}",
                    "enabled": true,
                    "canonicalValue": false,
                    "downStreamDefaultValue": true
                }
            ]
        }
    ],
    "localizations": [],
    "parents": ["rdm/lookupTypes/Country/{{COUNTRY_CODE}}"],
    "startDate": 0,
    "endDate": 0
  }'''


def write_to_file(codelist, out_filename, country):
    result = '[\n'
    result += ',\n'.join(map(lambda code: template
                             .replace('{{CODE}}', code[0])
                             .replace('{{VALUE}}', code[1])
                             .replace('{{COUNTRY_CODE}}', country)
                             , codelist))
    result += "\n]"

    with open(out_filename, 'w') as fout:
        fout.write(result.strip())


def generate(country):
    in_filename = country.upper() + '.csv'

    buf_limit = 100
    buf = []
    part = 0
    with open(in_filename) as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            buf.append(row)
            if len(buf) >= buf_limit:
                write_to_file(buf, '../../RdmLookups/State/%s_state_%d-%d.json' %
                              (country, part * buf_limit + 1, part * buf_limit + len(buf)), country.upper())
                buf = []
                part += 1

    if buf:
        write_to_file(buf, '../../RdmLookups/State/%s_state_%d-%d.json' %
                      (country, part * buf_limit + 1, part * buf_limit + len(buf)), country.upper())


generate("ca")
